package Vue;

import Controlleur.Controller;
import Controlleur.DataTrimer;
import Controlleur.MainWindowButtons;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainWindow extends Stage{

    private Controller mainControl;
    private MainWindowButtons mainWindowButtons = new MainWindowButtons();

    private DataTrimer trimAction = new DataTrimer();

    private GridPane mainPane = new GridPane();
    private Scene mainScene;
    private Stage mainWindow;

    private Button showAllUnitsButton;
    private Button teamBuilderButton;

    private Button addOneUnitButton;
    private Button editOneUnitButton;
    private Button LoadDataButton;

    private Button goToSimulationWindowButton;

    private Label existingUnits;
    private Label existingSpheres;
    private Label existingElgifs;


    public MainWindow(Controller mainControl){
        this.mainControl = mainControl;
    }

    public void startApplication(){

        mainWindow = this;

        setMainWindowSettings();

        mainPane.setHgap(15);
        mainPane.setVgap(25);

        setLoadDataButton("Load data");
        setshowAllUnitsButton("Show units");
        setTeamBuilderButton("Build Team");

        mainPane.addRow(1, LoadDataButton);
        mainPane.addRow(2, showAllUnitsButton);
        mainPane.addRow(3, teamBuilderButton);

        mainScene = new Scene(mainPane);
        mainWindow.setScene(mainScene);
        mainWindow.show();

    }

    private void setMainWindowSettings(){

        mainWindow.setTitle("Solveur Brave Frontier");
        mainWindow.setX(200);
        mainWindow.setY(200);
        mainWindow.setHeight(600);
        mainWindow.setWidth(600);

    }

    private void setTeamBuilderButton(String label){

        teamBuilderButton = new Button(label);
        teamBuilderButton.setOnAction(mainWindowButtons.goToTeamBuilderWindow(mainControl, this.mainWindow));
    }

    private void setLoadDataButton(String label){

        LoadDataButton = new Button(label);
        LoadDataButton.setOnAction(mainWindowButtons.trimDataButton(mainControl));


    }

    private void setshowAllUnitsButton(String label){

        showAllUnitsButton = new Button(label);
        showAllUnitsButton.setOnAction(mainWindowButtons.goToShowUnitsWindow(mainControl, this.mainWindow));

    }

    public void setAddOneUnitButton(Button addOneUnitButton) {
        this.addOneUnitButton = addOneUnitButton;
    }

    public void setEditOneUnitButton(Button editOneUnitButton) {
        this.editOneUnitButton = editOneUnitButton;
    }

}
