package Vue;

import Controlleur.Controller;
import Donnee.EffectIntermediaire;
import Donnee.Personnage;
import Donnee.SimpleEffect;
import Donnee.TeamBuilderEffect;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.*;

public class TeamBuilderWindow extends Stage {

    static int count = 0;


    private Controller mainController;
    private ListView listUnit = new ListView();
    private ListView listSelectedUnit = new ListView();
    private VBox vBoxButton = new VBox();
    private VBox vBoxStatUnit = new VBox();
    private Accordion buffList = new Accordion();

    private HBox unitSelection = new HBox();

    private Button addToSelectedButton;
    private Button removeFromSelectedButton;

    private BorderPane mainPane = new BorderPane();
    private Scene mainScene;
    private Stage stage;


    public TeamBuilderWindow(Controller mainController){
        this.mainController = mainController;
    }


    public void initTeamBuilderWindow() {

        stage = this;
        stage.setTitle("Solveur Brave Frontier");
        stage.setX(200);
        stage.setY(200);
        stage.setHeight(800);
        stage.setWidth(1200);


        listUnit.getItems().addAll(mainController.getUnitsList());

        listUnit.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listSelectedUnit.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        setAddToSelectedButton();
        setRemoveFromSelectedButton();

        vBoxButton.getChildren().add(addToSelectedButton);
        vBoxButton.getChildren().add(new Label("   "));
        vBoxButton.getChildren().add(removeFromSelectedButton);

        unitSelection.getChildren().add(listUnit);
        unitSelection.getChildren().add(new Label("   "));
        unitSelection.getChildren().add(vBoxButton);
        unitSelection.getChildren().add(new Label("   "));
        unitSelection.getChildren().add(listSelectedUnit);


        mainPane.setLeft(unitSelection);
        mainPane.setRight(buffList);

        mainScene = new Scene(mainPane);
        stage.setScene(mainScene);
        stage.show();

    }


    public void setAccordion(List<Personnage> unitList){

        TitledPane ttpSP = new TitledPane();
        ttpSP.setText("Skill points");

        TitledPane ttpLS = new TitledPane();
        ttpLS.setText("Leader skill");

        TitledPane ttpES = new TitledPane();
        ttpES.setText("Extra skill");

        TitledPane ttpBB = new TitledPane();
        ttpBB.setText("Brave burst");

        TitledPane ttpUBB = new TitledPane();
        ttpUBB.setText("Ultimate brave burst");

//        ttpSP.setContent(setSpTable(unitList));
//        ttpLS.setContent(setLSTable(unitList));
//        ttpES.setContent(setESTable(unitList));

        ttpBB.setContent(setBBTable(unitList));
//        ttpUBB.setContent(setUBBTable(unitList));

        buffList.getPanes().addAll(ttpBB);
        buffList.setPrefWidth(800);

    }

    public TableView setTable(){

        TableView table = new TableView();

        TableColumn unitName = new TableColumn("Unit name");
        unitName.setCellValueFactory( new PropertyValueFactory<TeamBuilderEffect, String>("unitName"));

        TableColumn origineBuff = new TableColumn("Origine Buff");
        origineBuff.setCellValueFactory( new PropertyValueFactory<TeamBuilderEffect, String>("origineBuff"));

        TableColumn buffType = new TableColumn("Type buff");
        buffType.setCellValueFactory( new PropertyValueFactory<TeamBuilderEffect, String>("name"));

        TableColumn buffValue = new TableColumn("Value");
        buffValue.setCellValueFactory( new PropertyValueFactory<TeamBuilderEffect, Double>("PercentValue"));

        TableColumn buffDuration = new TableColumn("Turn duration");
        buffDuration.setCellValueFactory( new PropertyValueFactory<TeamBuilderEffect, Double>("numberOfTurns"));

        TableColumn buffTargets = new TableColumn("Targets");
        buffTargets.setCellValueFactory( new PropertyValueFactory<TeamBuilderEffect, Double>("target"));

        TableColumn buffProbability = new TableColumn("Probability to occur");
        buffProbability.setCellValueFactory( new PropertyValueFactory<TeamBuilderEffect, Double>("PercentProbablity"));

        TableColumn alreadyInList = new TableColumn("Already in list");
        alreadyInList.setCellValueFactory( new PropertyValueFactory<TeamBuilderEffect, Boolean>("alreadyInList"));

        table.getColumns().addAll(unitName, origineBuff, buffType,buffValue,buffDuration,buffTargets,buffProbability,alreadyInList);

        return table;
    }

    public TableView setSpTable(List<Personnage> unitList){

        TableView table = setTable();

        for (Personnage unit : unitList) {

            for (SimpleEffect simple : unit.getSkillPoints().getSimpleEffectList()) {
                TeamBuilderEffect t = new TeamBuilderEffect();

                t.setUnitName(unit.getName());
                t.setOrigineBuff("SP");

                t.setName(simple.getName());
                t.setPercentValue(simple.getPercentValue());
                t.setNumberOfTurns(simple.getNumberOfTurns());
                t.setTarget(simple.getTarget());
                t.setPercentProbablity(simple.getPercentProbablity());

                table.getItems().add(t);
            }

        }



        table.setMaxHeight((table.getItems().size() +1) * 30);

        return table;
    }


    public TableView setLSTable(List<Personnage> unitList){

        TableView table = setTable();


        for (Personnage unit : unitList) {


            for (SimpleEffect simple : unit.getLeaderSkill().getSimpleEffectList()) {
                TeamBuilderEffect t = new TeamBuilderEffect();

                t.setUnitName(unit.getName());
                t.setOrigineBuff("LS");

                t.setName(simple.getName());
                t.setPercentValue(simple.getPercentValue());
                t.setNumberOfTurns(simple.getNumberOfTurns());
                t.setTarget(simple.getTarget());
                t.setPercentProbablity(simple.getPercentProbablity());

                table.getItems().add(t);
            }
        }

        table.setMaxHeight((table.getItems().size() +1) * 30);


        return table;
    }

    public TableView setESTable(List<Personnage> unitList){

        TableView table = setTable();

            for (Personnage unit : unitList) {


                for (SimpleEffect simple : unit.getExtraSkill().getSimpleEffectList()) {
                    TeamBuilderEffect t = new TeamBuilderEffect();

                    t.setUnitName(unit.getName());
                    t.setOrigineBuff("ES");

                    t.setName(simple.getName());
                    t.setPercentValue(simple.getPercentValue());
                    t.setNumberOfTurns(simple.getNumberOfTurns());
                    t.setTarget(simple.getTarget());
                    t.setPercentProbablity(simple.getPercentProbablity());

                    table.getItems().add(t);
                }
            }

        table.setMaxHeight((table.getItems().size() +1) * 30);


        return table;
    }

    public TableView setBBTable(List<Personnage> unitList){

        TableView table = setTable();

        ArrayList<TeamBuilderEffect> listTeamEffect = new ArrayList<>();

        for (Personnage unit : unitList) {


            for (SimpleEffect simple : unit.getBraveBurst().getSimpleEffectList()) {
                TeamBuilderEffect t = new TeamBuilderEffect();

                t.setUnitName(unit.getName());
                t.setOrigineBuff("BB");

                t.setName(simple.getName());
                t.setPercentValue(simple.getPercentValue());
                t.setNumberOfTurns(simple.getNumberOfTurns());
                t.setTarget(simple.getTarget());
                t.setPercentProbablity(simple.getPercentProbablity());

                listTeamEffect.add(t);
            }


            for (SimpleEffect simple : unit.getSuperBraveBurst().getSimpleEffectList()) {
                TeamBuilderEffect t = new TeamBuilderEffect();

                t.setUnitName(unit.getName());
                t.setOrigineBuff("SBB");

                t.setName(simple.getName());
                t.setPercentValue(simple.getPercentValue());
                t.setNumberOfTurns(simple.getNumberOfTurns());
                t.setTarget(simple.getTarget());
                t.setPercentProbablity(simple.getPercentProbablity());

                listTeamEffect.add(t);
            }
        }

        for(int i = 0; i< listTeamEffect.size()-1; i++){

            ArrayList<EffectIntermediaire> listName = new ArrayList<>();
            ArrayList<Integer> listIndex = new ArrayList<>();

            if(listName.isEmpty()){
                EffectIntermediaire e = new EffectIntermediaire();
                e.setNameBuff(listTeamEffect.get(i).getName());
                e.setTargets(listTeamEffect.get(i).getTarget());
                listName.add(e);
                listIndex.add(i);
            }

            if(listTeamEffect.get(i).getName().contains("ignore_defense")){
                System.out.println("on y est");
            }

            for(int j = i+1; j < listTeamEffect.size(); j++){
                if(listTeamEffect.get(j).getName().contains("ignore_defense")){
                    System.out.println("on y est");
                }
                if(listTeamEffect.get(j).getName().equals(listName.get(0).getNameBuff()) &&
                        listTeamEffect.get(j).getTarget() == listName.get(0).getTargets()){

                   EffectIntermediaire e = new EffectIntermediaire();
                   e.setNameBuff(listTeamEffect.get(j).getName());
                   e.setTargets(listTeamEffect.get(j).getTarget());
                   listName.add(e);
                   listIndex.add(j);
                }
            }

            if(listIndex.size() > 1){
                for (int j = 0; j < listIndex.size() ; j++) {
                    listTeamEffect.get(listIndex.get(j)).setAlreadyInList(true);
                }
            }

        }




//        for (EffectIntermediaire te: tempEffect) {
//
//            System.out.println(te.getNameBuff());
//            System.out.println(te.getTargets());
//            System.out.println("*----------------");
//
//        }

//        ArrayList<Boolean> trueList = new ArrayList<>();
//
//        for(TeamBuilderEffect te : listTeamEffect){
//            trueList.add(false);
//        }
//
//        for (int i = 0; i <listTeamEffect.size() ; i++) {
//            for (int j = i; j < listTeamEffect.size() ; j++) {
//
//                System.out.println(listTeamEffect.get(i).getName() == listTeamEffect.get(j).getName());
//
//                if(listTeamEffect.get(i).getName() == listTeamEffect.get(j).getName()
//                        && listTeamEffect.get(i).getTarget() == listTeamEffect.get(j).getTarget()){
//
//                    listTeamEffect.get(i).setAlreadyInList(true);
//                    listTeamEffect.get(j).setAlreadyInList(true);
//
//                }
//            }
//        }


        table.setRowFactory( tv -> new TableRow<TeamBuilderEffect>(){
            @Override
            protected void updateItem(TeamBuilderEffect teamEffect, boolean empty){
                super.updateItem(teamEffect, empty);
                if (teamEffect == null)
                    setStyle("");
                else if (teamEffect.isAlreadyInList())
                    setStyle("-fx-background-color: #ff9494;");
                else
                    setStyle("-fx-background-color: #97ff87;");
            }
        });



        for (TeamBuilderEffect effect : listTeamEffect) {
            table.getItems().add(effect);

        }

        table.setMaxHeight((table.getItems().size() +1) * 30);

        return table;
    }

    public boolean buffBBExist(ArrayList<TeamBuilderEffect> listEffect,ArrayList<Boolean> trueList, TeamBuilderEffect teamEffect){

        return trueList.get(listEffect.indexOf(teamEffect));

    }

    public TableView setSBBTable(Personnage unit){

        TableView table = setTable();

        for (SimpleEffect simple : unit.getSuperBraveBurst().getSimpleEffectList()) {
            table.getItems().add(simple);
        }

        table.setMaxHeight((table.getItems().size() +1) * 30);


        return table;
    }

    public TableView setUBBTable(List<Personnage> unitList){

        TableView table = setTable();

        for (Personnage unit : unitList) {

            for (SimpleEffect simple : unit.getUltimateBraveBurst().getSimpleEffectList()) {
                TeamBuilderEffect t = new TeamBuilderEffect();

                t.setUnitName(unit.getName());
                t.setOrigineBuff("UBB");

                t.setName(simple.getName());
                t.setPercentValue(simple.getPercentValue());
                t.setNumberOfTurns(simple.getNumberOfTurns());
                t.setTarget(simple.getTarget());
                t.setPercentProbablity(simple.getPercentProbablity());

                table.getItems().add(t);
            }

        }
        table.setMaxHeight((table.getItems().size() +1) * 30);


        return table;
    }


    public void setAddToSelectedButton() {
        addToSelectedButton = new Button("->");
        addToSelectedButton.setOnAction(addUnitToSelection());
    }

    public void setRemoveFromSelectedButton() {
        removeFromSelectedButton = new Button("<-");
        removeFromSelectedButton.setOnAction(removeUnitFromSelection());
    }

    public EventHandler<ActionEvent> addUnitToSelection() {

        EventHandler<ActionEvent> a = new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                listSelectedUnit.getItems().addAll(listUnit.getSelectionModel().getSelectedItems());
                listUnit.getItems().removeAll(listUnit.getSelectionModel().getSelectedItems());

                setAccordion( listSelectedUnit.getItems());

            }
        };
        return a;
    }

    public EventHandler<ActionEvent> removeUnitFromSelection() {

        EventHandler<ActionEvent> a = new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                listUnit.getItems().addAll(listSelectedUnit.getSelectionModel().getSelectedItems());
                listSelectedUnit.getItems().removeAll(listSelectedUnit.getSelectionModel().getSelectedItems());

                setAccordion( listSelectedUnit.getItems());
            }
        };
        return a;
    }

}
