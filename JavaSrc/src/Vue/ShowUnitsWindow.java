package Vue;

import Controlleur.Controller;
import Donnee.Personnage;
import Donnee.SimpleEffect;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.beans.EventHandler;


public class ShowUnitsWindow extends Stage{

    private Controller mainController;
    private ListView listUnit = new ListView();
    private VBox vBoxUnitList = new VBox();
    private VBox vBoxStatUnit = new VBox();
    private Accordion buffList = new Accordion();


    private BorderPane mainPane = new BorderPane();
    private Scene mainScene;
    private Stage stage;


    public ShowUnitsWindow(Controller mainController) {
        this.mainController = mainController;
    }

    public void initShowUnitsWindow(){

        stage = this;
        stage.setTitle("Solveur Brave Frontier");
        stage.setX(200);
        stage.setY(200);
        stage.setHeight(800);
        stage.setWidth(1200);

        listUnit.getItems().addAll(mainController.getUnitsList());


        listUnit.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener() {
                    @Override
                    public void changed(ObservableValue observableValue, Object o, Object t1) {

                        String nom = listUnit.getSelectionModel().getSelectedItem().toString();
                        Personnage selectedUnit = new Personnage();

                        for (Personnage unit : mainController.getUnitsList()) {
                            if(unit.getName().equals(nom)) {
                                selectedUnit = unit;
                                break;
                            }
                        }

                        buffList.getPanes().clear();
                        setAccordion(selectedUnit);
                        buffList.getPanes().get(0).setExpanded(true);

                    }
                }
        );


        vBoxStatUnit.getChildren().add(buffList);
        vBoxUnitList.getChildren().add(listUnit);

        mainPane.setLeft(vBoxUnitList);
        mainPane.setRight(vBoxStatUnit);

        mainScene = new Scene(mainPane);
        stage.setScene(mainScene);
        stage.show();

    }


    public void setAccordion(Personnage unit){

        TitledPane ttpSP = new TitledPane();
        ttpSP.setText("Skill points");

        TitledPane ttpLS = new TitledPane();
        ttpLS.setText("Leader skill");

        TitledPane ttpES = new TitledPane();
        ttpES.setText("Extra skill");

        TitledPane ttpBB = new TitledPane();
        ttpBB.setText("Brave burst");

        TitledPane ttpSBB = new TitledPane();
        ttpSBB.setText("Super brave burst");

        TitledPane ttpUBB = new TitledPane();
        ttpUBB.setText("Ultimate brave burst");

        ttpSP.setContent(setSpTable(unit));
        ttpLS.setContent(setLSTable(unit));
        ttpES.setContent(setESTable(unit));

        ttpBB.setContent(setBBTable(unit));
//        ttpBB.setMaxHeight(1200);
        ttpSBB.setContent(setSBBTable(unit));
        ttpUBB.setContent(setUBBTable(unit));

        buffList.getPanes().addAll(ttpSP,ttpLS,ttpES,ttpBB,ttpSBB,ttpUBB);
        buffList.setPrefWidth(500);
//        buffList.setMinHeight(800);

    }

    public TableView setTable(){

        TableView table = new TableView();

        TableColumn buffType = new TableColumn("Type buff");
        buffType.setCellValueFactory( new PropertyValueFactory<SimpleEffect, String>("name"));

        TableColumn buffValue = new TableColumn("Value");
        buffValue.setCellValueFactory( new PropertyValueFactory<SimpleEffect, Double>("PercentValue"));

        TableColumn buffDuration = new TableColumn("Turn duration");
        buffDuration.setCellValueFactory( new PropertyValueFactory<SimpleEffect, Double>("numberOfTurns"));

        TableColumn buffTargets = new TableColumn("Targets");
        buffTargets.setCellValueFactory( new PropertyValueFactory<SimpleEffect, Double>("target"));

        TableColumn buffProbability = new TableColumn("Probability to occur");
        buffProbability.setCellValueFactory( new PropertyValueFactory<SimpleEffect, Double>("PercentProbablity"));

        table.getColumns().addAll(buffType,buffValue,buffDuration,buffTargets,buffProbability);

        return table;
    }

    public TableView setSpTable(Personnage unit){

        TableView table = setTable();

        for (SimpleEffect simple : unit.getSkillPoints().getSimpleEffectList()) {
            table.getItems().add(simple);
        }

        table.setMaxHeight((table.getItems().size() +1) * 30);

        return table;
    }


    public TableView setLSTable(Personnage unit){

        TableView table = setTable();

        for (SimpleEffect simple : unit.getLeaderSkill().getSimpleEffectList()) {
            table.getItems().add(simple);
        }

        table.setMaxHeight((table.getItems().size() +1) * 30);


        return table;
    }

    public TableView setESTable(Personnage unit){

        TableView table = setTable();

        for (SimpleEffect simple : unit.getExtraSkill().getSimpleEffectList()) {
            table.getItems().add(simple);
        }

        table.setMaxHeight((table.getItems().size() +1) * 30);


        return table;
    }

    public TableView setBBTable(Personnage unit){

        TableView table = setTable();

        for (SimpleEffect simple : unit.getBraveBurst().getSimpleEffectList()) {
            table.getItems().add(simple);
        }

        table.setMaxHeight((table.getItems().size() +1) * 30);


        return table;
    }

    public TableView setSBBTable(Personnage unit){

        TableView table = setTable();

        for (SimpleEffect simple : unit.getSuperBraveBurst().getSimpleEffectList()) {
            table.getItems().add(simple);
        }

        table.setMaxHeight((table.getItems().size() +1) * 30);


        return table;
    }

    public TableView setUBBTable(Personnage unit){

        TableView table = setTable();

        for (SimpleEffect simple : unit.getUltimateBraveBurst().getSimpleEffectList()) {
            table.getItems().add(simple);
        }

        table.setMaxHeight((table.getItems().size() +1) * 30);


        return table;
    }

}
