package Donnee;

public class TeamBuilderEffect extends SimpleEffect{

    private String unitName;
    private String OrigineBuff;
    private Boolean alreadyInList = false;

    public Boolean isAlreadyInList() {
        return alreadyInList;
    }

    public void setAlreadyInList(Boolean alreadyInList) {
        this.alreadyInList = alreadyInList;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getOrigineBuff() {
        return OrigineBuff;
    }

    public void setOrigineBuff(String origineBuff) {
        OrigineBuff = origineBuff;
    }
}
