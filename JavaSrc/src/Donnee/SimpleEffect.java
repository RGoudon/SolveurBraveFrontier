package Donnee;

public class SimpleEffect {

    private String name;
    private Double flatStat;
    private Double PercentProbablity;
    private Integer numberOfTurns;
    private Integer target;
    private Double PercentValue;



    public void setName(String name) {
        this.name = name;
    }

    public void setPercentProbablity(Double percentProbablity) {
        PercentProbablity = percentProbablity;
    }

    public void setNumberOfTurns(Integer numberOfTurns) {
        this.numberOfTurns = numberOfTurns;
    }

    public void setTarget(Integer target) {
        this.target = target;
    }

    public void setPercentValue(Double percentValue) {
        PercentValue = percentValue;
    }

    public String getName() {
        return name;
    }

    public Double getPercentProbablity() {
        return PercentProbablity;
    }

    public Integer getNumberOfTurns() {
        return numberOfTurns;
    }

    public Integer getTarget() {
        return target;
    }

    public Double getPercentValue() {
        return PercentValue;
    }

}
