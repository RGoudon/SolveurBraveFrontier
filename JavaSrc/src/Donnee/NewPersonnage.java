package Donnee;

public class NewPersonnage {

    private String name;
    private String gender;
    private String element;

    private Long gameId;
    private Long guideId;

    private Stats stat;
    private Stats impStats;
    private Stats overDriveStats;


    private Burst braveBurst;
    private Burst superBraveBurst;
    private Burst ultimateBraveBurst;

    private Skill leaderSkill;
    private Skill extraSkill;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public Long getGuideId() {
        return guideId;
    }

    public void setGuideId(Long guideId) {
        this.guideId = guideId;
    }

    public Stats getStat() {
        return stat;
    }

    public void setStat(Stats stat) {
        this.stat = stat;
    }

    public Stats getImpStats() {
        return impStats;
    }

    public void setImpStats(Stats impStats) {
        this.impStats = impStats;
    }

    public Stats getOverDriveStats() {
        return overDriveStats;
    }

    public void setOverDriveStats(Stats overDriveStats) {
        this.overDriveStats = overDriveStats;
    }

    public Burst getBraveBurst() {
        return braveBurst;
    }

    public void setBraveBurst(Burst braveBurst) {
        this.braveBurst = braveBurst;
    }

    public Burst getSuperBraveBurst() {
        return superBraveBurst;
    }

    public void setSuperBraveBurst(Burst superBraveBurst) {
        this.superBraveBurst = superBraveBurst;
    }

    public Burst getUltimateBraveBurst() {
        return ultimateBraveBurst;
    }

    public void setUltimateBraveBurst(Burst ultimateBraveBurst) {
        this.ultimateBraveBurst = ultimateBraveBurst;
    }

    public Skill getLeaderSkill() {
        return leaderSkill;
    }

    public void setLeaderSkill(Skill leaderSkill) {
        this.leaderSkill = leaderSkill;
    }

    public Skill getExtraSkill() {
        return extraSkill;
    }

    public void setExtraSkill(Skill extraSkill) {
        this.extraSkill = extraSkill;
    }
}
