package Donnee;

import java.util.ArrayList;

public class CurrentBuff {

    private BasicListBuff skillPoints;
    private BasicListBuff leaderSkill;
    private BasicListBuff extraSkill;

    private BasicListBuff bb_sbb_buffs;
    private BasicListBuff ultimateBraveBurst;

    private BasicListBuff sphere1;
    private BasicListBuff sphere2;

    private BasicListBuff elgif;

    private ArrayList<String> special = new ArrayList<>();

    public BasicListBuff getSkillPoints() {
        return skillPoints;
    }

    public BasicListBuff getLeaderSkill() {
        return leaderSkill;
    }

    public BasicListBuff getExtraSkill() {
        return extraSkill;
    }

    public BasicListBuff getBb_sbb_buffs() {
        return bb_sbb_buffs;
    }

    public BasicListBuff getUltimateBraveBurst() {
        return ultimateBraveBurst;
    }

    public BasicListBuff getSphere1() {
        return sphere1;
    }

    public BasicListBuff getSphere2() {
        return sphere2;
    }

    public BasicListBuff getElgif() {
        return elgif;
    }

    public ArrayList<String> getSpecial() {
        return special;
    }

    public void setSkillPoints(BasicListBuff skillPoints) {
        this.skillPoints = skillPoints;
    }

    public void setLeaderSkill(BasicListBuff leaderSkill) {
        this.leaderSkill = leaderSkill;
    }

    public void setExtraSkill(BasicListBuff extraSkill) {
        this.extraSkill = extraSkill;
    }

    public void setBb_sbb_buffs(BasicListBuff bb_sbb_buffs) {
        this.bb_sbb_buffs = bb_sbb_buffs;
    }

    public void setUltimateBraveBurst(BasicListBuff ultimateBraveBurst) {
        this.ultimateBraveBurst = ultimateBraveBurst;
    }

    public void setSphere1(BasicListBuff sphere1) {
        this.sphere1 = sphere1;
    }

    public void setSphere2(BasicListBuff sphere2) {
        this.sphere2 = sphere2;
    }

    public void setElgif(BasicListBuff elgif) {
        this.elgif = elgif;
    }

    public void setSpecial(ArrayList<String> special) {
        this.special = special;
    }
}
