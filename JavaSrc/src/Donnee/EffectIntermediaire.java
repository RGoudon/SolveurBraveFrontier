package Donnee;

public class EffectIntermediaire {

    private String nameBuff;
    private Integer targets;

    public String getNameBuff() {
        return nameBuff;
    }

    public void setNameBuff(String nameBuff) {
        this.nameBuff = nameBuff;
    }

    public Integer getTargets() {
        return targets;
    }

    public void setTargets(Integer targets) {
        this.targets = targets;
    }
}
