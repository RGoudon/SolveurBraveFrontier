package Donnee;

import java.util.ArrayList;

public class BasicListBuff {

    private ArrayList<SimpleEffect> simpleEffectList = new ArrayList<>();

    //add effect to currentBuffs on unit, if same type already in list, replace it with new one and return true, else return false
    public boolean addEffectToCurrentBuff(SimpleEffect simpleEffect){
        for (SimpleEffect se : simpleEffectList) {

            if(se.getName() == simpleEffect.getName() && se.getTarget() == simpleEffect.getTarget()){
                simpleEffectList.remove(simpleEffectList.indexOf(se));
                simpleEffectList.add(simpleEffect);
                return true;
            }
        }
        return false;
    }

    public void addEffectToList(SimpleEffect simpleEffect){
        simpleEffectList.add(simpleEffect);
    }

    public ArrayList<SimpleEffect> getSimpleEffectList() {
        return simpleEffectList;
    }
}
