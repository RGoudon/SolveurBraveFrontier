package Donnee;

import java.util.ArrayList;

public class Personnage {

    private String name;
    private String gender;
    private String type;

    private Double attack;
    private Double defense;
    private Double recuperation;
    private Double hp;

    private Double impAttack;
    private Double impDefense;
    private Double impRecuperation;
    private Double impHp;

    private BasicListBuff skillPoints = new BasicListBuff();
    private BasicListBuff leaderSkill = new BasicListBuff();
    private BasicListBuff extraSkill = new BasicListBuff();

    private BasicListBuff braveBurst = new BasicListBuff();
    private BasicListBuff superBraveBurst = new BasicListBuff();
    private BasicListBuff ultimateBraveBurst = new BasicListBuff();

    private BasicListBuff sphere1 = new BasicListBuff();
    private BasicListBuff sphere2 = new BasicListBuff();

    private BasicListBuff elgif = new BasicListBuff();

    private ArrayList<String> special = new ArrayList<>();

    private CurrentBuff currentBuff;

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getType() {
        return type;
    }

    public Double getAttack() {
        return attack;
    }

    public Double getDefense() {
        return defense;
    }

    public Double getRecuperation() {
        return recuperation;
    }

    public Double getHp() {
        return hp;
    }

    public Double getImpAttack() {
        return impAttack;
    }

    public Double getImpDefense() {
        return impDefense;
    }

    public Double getImpRecuperation() {
        return impRecuperation;
    }

    public Double getImpHp() {
        return impHp;
    }

    public BasicListBuff getSkillPoints() {
        return skillPoints;
    }

    public BasicListBuff getLeaderSkill() {
        return leaderSkill;
    }

    public BasicListBuff getExtraSkill() {
        return extraSkill;
    }

    public BasicListBuff getBraveBurst() {
        return braveBurst;
    }

    public BasicListBuff getSuperBraveBurst() {
        return superBraveBurst;
    }

    public BasicListBuff getUltimateBraveBurst() {
        return ultimateBraveBurst;
    }

    public BasicListBuff getSphere1() {
        return sphere1;
    }

    public BasicListBuff getSphere2() {
        return sphere2;
    }

    public BasicListBuff getElgif() {
        return elgif;
    }

    public ArrayList<String> getSpecial() {
        return special;
    }

    public CurrentBuff getCurrentBuff() {
        return currentBuff;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAttack(Double attack) {
        this.attack = attack;
    }

    public void setDefense(Double defense) {
        this.defense = defense;
    }

    public void setRecuperation(Double recuperation) {
        this.recuperation = recuperation;
    }

    public void setHp(Double hp) {
        this.hp = hp;
    }

    public void setImpAttack(Double impAttack) {
        this.impAttack = impAttack;
    }

    public void setImpDefense(Double impDefense) {
        this.impDefense = impDefense;
    }

    public void setImpRecuperation(Double impRecuperation) {
        this.impRecuperation = impRecuperation;
    }

    public void setImpHp(Double impHp) {
        this.impHp = impHp;
    }

    public void setSkillPoints(BasicListBuff skillPoints) {
        this.skillPoints = skillPoints;
    }

    public void setLeaderSkill(BasicListBuff leaderSkill) {
        this.leaderSkill = leaderSkill;
    }

    public void setExtraSkill(BasicListBuff extraSkill) {
        this.extraSkill = extraSkill;
    }

    public void setBraveBurst(BasicListBuff braveBurst) {
        this.braveBurst = braveBurst;
    }

    public void setSuperBraveBurst(BasicListBuff superBraveBurst) {
        this.superBraveBurst = superBraveBurst;
    }

    public void setUltimateBraveBurst(BasicListBuff ultimateBraveBurst) {
        this.ultimateBraveBurst = ultimateBraveBurst;
    }

    public void setSphere1(BasicListBuff sphere1) {
        this.sphere1 = sphere1;
    }

    public void setSphere2(BasicListBuff sphere2) {
        this.sphere2 = sphere2;
    }

    public void setElgif(BasicListBuff elgif) {
        this.elgif = elgif;
    }

    public void setSpecial(ArrayList<String> special) {
        this.special = special;
    }

    public void addToSPList(SimpleEffect se){

        skillPoints.addEffectToList(se);

    }

    public void addToLSList(SimpleEffect se){

        leaderSkill.addEffectToList(se);

    }

    public void addToESList(SimpleEffect se){

        extraSkill.addEffectToList(se);

    }

    public void addToBBList(SimpleEffect se){
        braveBurst.addEffectToList(se);
    }

    public void addToSBBList(SimpleEffect se){
        superBraveBurst.addEffectToList(se);
    }

    public void addToUBBList(SimpleEffect se) {
        ultimateBraveBurst.addEffectToList(se);
    }
}
