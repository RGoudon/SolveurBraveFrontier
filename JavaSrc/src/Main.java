import Controlleur.Controller;
import Controlleur.DataTrimer;
import Controlleur.JSonLoader;
import Vue.MainWindow;
import javafx.application.Application;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.*;


public class Main extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception{

        JSonLoader j = new JSonLoader();

        j.openJson();
        j.putRawJsonInForm();

//        Controller mainController = new Controller();
//        MainWindow applicationWindow = new MainWindow(mainController);
//        applicationWindow.startApplication();

    }


    public static void main(String[] args) {
//        System.out.println("test");
        launch(args);
    }


}
