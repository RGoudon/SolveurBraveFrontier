package Controlleur;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLOutput;
import java.util.Scanner;

public class DataTrimer {

    private final String pathFilesToTrim = "C:\\Users\\gloumii\\Desktop\\BraveFrontier\\SolveurBraveFrontier\\Data\\DataToTrim";
    private final String pathFilesTrimed = "C:\\Users\\gloumii\\Desktop\\BraveFrontier\\SolveurBraveFrontier\\Data\\DataReady";

    public void startProcess() {

        File directory = new File(pathFilesToTrim);
        String[] listOfFiles = directory.list();
        System.out.println(listOfFiles);
        for (String fileName : listOfFiles) {
            trimFile(fileName);

        }

    }

    public void trimFile(String fileName){

        try {

            File currentFile = new File(pathFilesToTrim + "\\" + fileName);
            Scanner scanTrimer = new Scanner(currentFile);

            FileWriter fileTrimed = new FileWriter(pathFilesTrimed + "\\" + fileName);

            String currentLine;

            while(scanTrimer.hasNextLine()){
                currentLine = scanTrimer.nextLine();
                if(!currentLine.contains(",0,0,0,0") && !currentLine.isEmpty()){
                    fileTrimed.write(currentLine +"\n");
                }
            }
            fileTrimed.close();
            scanTrimer.close();

        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
