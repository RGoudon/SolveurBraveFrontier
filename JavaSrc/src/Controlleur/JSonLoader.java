package Controlleur;

import Donnee.NewPersonnage;
import Donnee.Personnage;
import netscape.javascript.JSObject;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class JSonLoader {

    private String jsonPath = "C:\\Users\\gloumii\\Desktop\\BraveFrontier\\SolveurBraveFrontier\\Data\\allUnitsInfo.json";
    private String rawJson;

    public void openJson() throws IOException {
        FileInputStream inputStream = null;
        Scanner sc = null;
        try {
            inputStream = new FileInputStream(jsonPath);
            sc = new Scanner(inputStream, "UTF-8");
            long count = 0;

            StringBuilder line = new StringBuilder("");

            while (sc.hasNextLine()) {
                line.append(sc.nextLine());
                // System.out.println(line);
                count++;
                if (count % 100000 == 0) {
                    System.out.println(count + " lignes traitées");
                }
            }

            // note that Scanner suppresses exceptions
            if (sc.ioException() != null) {
                throw sc.ioException();
            }

            rawJson = line.toString();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            if (inputStream != null) {
                inputStream.close();
            }
            if (sc != null) {
                sc.close();
            }
        }

    }


    public void putRawJsonInForm(){
        JSONObject jsonObject = new JSONObject(rawJson);
        JSONArray listBaseIdUnit = jsonObject.names();

        ArrayList<String> listNameUnits = new ArrayList<>();

        ArrayList<NewPersonnage> listPersonnage = new ArrayList<>();

        for (Object baseId : listBaseIdUnit) {

            NewPersonnage personnage = new NewPersonnage();

            personnage.setName(jsonObject.getJSONObject(baseId.toString()).getString("name"));
            personnage.setGender(jsonObject.getJSONObject(baseId.toString()).getString("gender"));
            personnage.setElement(jsonObject.getJSONObject(baseId.toString()).getString("element"));

            personnage.setGameId(jsonObject.getJSONObject(baseId.toString()).getLong("id"));
            personnage.setGuideId(jsonObject.getJSONObject(baseId.toString()).getLong("guide_id"));

//            private Stats stat;
//            private Stats impStats;
//            private Stats overDriveStats;


//            private Burst braveBurst;
//            private Burst superBraveBurst;
//            private Burst ultimateBraveBurst;
//
//            private Skill leaderSkill;
//            private Skill extraSkill;

            listPersonnage.add(personnage);

        }

        System.out.println(listPersonnage.size());

        for (NewPersonnage perso : listPersonnage){
            System.out.println(perso.getName());
        }

    }
}
