package Controlleur;

import Vue.ShowUnitsWindow;
import Vue.TeamBuilderWindow;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;

public class MainWindowButtons {

    public EventHandler<ActionEvent> goToShowUnitsWindow(Controller mainControl, Stage stage) {

        EventHandler<ActionEvent> a = new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                stage.close();
//                System.out.println("close Fonctionne");
                ShowUnitsWindow newStage = new ShowUnitsWindow(mainControl);
                newStage.initShowUnitsWindow();
            }
        };
        return a;
    }


    public EventHandler<ActionEvent> goToTeamBuilderWindow(Controller mainControl, Stage stage) {

        EventHandler<ActionEvent> a = new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                stage.close();

                TeamBuilderWindow newStage = new TeamBuilderWindow(mainControl);
                newStage.initTeamBuilderWindow();
            }
        };
        return a;
    }

    public EventHandler<ActionEvent> trimDataButton(Controller mainControl) {

        EventHandler<ActionEvent> a = new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {

                DataTrimer data = new DataTrimer();

                data.startProcess();
//                System.out.println("Trim done");
                mainControl.initExistingData();
            }
        };
        return a;
    }


}
