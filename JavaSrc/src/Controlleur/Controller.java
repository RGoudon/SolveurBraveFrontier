package Controlleur;

import Donnee.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Controller {

    private ArrayList<Personnage> unitsList = new ArrayList<>();
    private ArrayList<Sphere> spheresList = new ArrayList<>();
    private ArrayList<Elgif> elgifsList = new ArrayList<>();

    private final String pathFilesToTrim = "C:\\Users\\gloumii\\Desktop\\BraveFrontier\\SolveurBraveFrontier\\Data\\DataToTrim";
    private final String pathFilesTrimed = "C:\\Users\\gloumii\\Desktop\\BraveFrontier\\SolveurBraveFrontier\\Data\\DataReady";

    public void initExistingData() {

        initUnitsList();

    }


    private void initUnitsList() {

        /* Open all units file, read them, add units to list */

        File directory = new File(pathFilesTrimed);
        String[] listOfFiles = directory.list();


        for (String fileName : listOfFiles) {
            unitsList.add((readUnitFile(fileName)));
        }

    }

    private Personnage readUnitFile(String fileName) {

        try {


            ArrayList<String> unitString = new ArrayList<>();
            File currentFile = new File(pathFilesTrimed + "\\" + fileName);
            Scanner fileReader = new Scanner(currentFile);
            Personnage currentUnit = new Personnage();

            while (fileReader.hasNextLine()) {
                unitString.add(fileReader.nextLine());
            }



            for (String field : unitString) {
//                System.out.println(field);
                field = field.replace(" ", "");
                String line[] = field.split(",");
                if (line[0].contains("nom")) {
                    currentUnit.setName(line[1]);
                } else if (line[0].contains("sexe")) {
                    currentUnit.setGender(line[1]);
                } else if (line[0].contains("type")) {
                    currentUnit.setType(line[1]);

                } else if (line[0].contains("stat")) {
                    currentUnit.setAttack((Double.parseDouble(line[1])));
                    currentUnit.setDefense((Double.parseDouble(line[2])));
                    currentUnit.setRecuperation((Double.parseDouble(line[3])));
                    currentUnit.setHp((Double.parseDouble(line[4])));

                } else if (line[0].contains("imp")) {
                    currentUnit.setImpAttack((Double.parseDouble(line[1])));
                    currentUnit.setImpDefense(Double.parseDouble(line[2]));
                    currentUnit.setImpRecuperation((Double.parseDouble(line[3])));
                    currentUnit.setImpHp((Double.parseDouble(line[4])));

                } else if (line[0].contains("SP")) {

                    SimpleEffect simpleEffect = new SimpleEffect();
                    simpleEffect.setName(line[1]);
                    simpleEffect.setPercentValue(Double.parseDouble(line[2]));
                    simpleEffect.setNumberOfTurns(Integer.parseInt(line[3]));
                    simpleEffect.setTarget(Integer.parseInt(line[4]));
                    simpleEffect.setPercentProbablity(Double.parseDouble(line[5]));
                    currentUnit.addToSPList(simpleEffect);

                } else if (line[0].contains("LS")) {

                    SimpleEffect simpleEffect = new SimpleEffect();
                    simpleEffect.setName(line[1]);
                    simpleEffect.setPercentValue(Double.parseDouble(line[2]));
                    simpleEffect.setNumberOfTurns(Integer.parseInt(line[3]));
                    simpleEffect.setTarget(Integer.parseInt(line[4]));
                    simpleEffect.setPercentProbablity(Double.parseDouble(line[5]));
                    currentUnit.addToLSList(simpleEffect);

                } else if (line[0].contains("ES")) {

                    SimpleEffect simpleEffect = new SimpleEffect();
                    simpleEffect.setName(line[1]);
                    simpleEffect.setPercentValue(Double.parseDouble(line[2]));
                    simpleEffect.setNumberOfTurns(Integer.parseInt(line[3]));
                    simpleEffect.setTarget(Integer.parseInt(line[4]));
                    simpleEffect.setPercentProbablity(Double.parseDouble(line[5]));
                    currentUnit.addToESList(simpleEffect);

                } else if (line[0].contains("SBB")) {

                    SimpleEffect simpleEffect = new SimpleEffect();
                    simpleEffect.setName(line[1]);
                    simpleEffect.setPercentValue(Double.parseDouble(line[2]));
                    simpleEffect.setNumberOfTurns(Integer.parseInt(line[3]));
                    simpleEffect.setTarget(Integer.parseInt(line[4]));
                    simpleEffect.setPercentProbablity(Double.parseDouble(line[5]));
                    currentUnit.addToSBBList(simpleEffect);

                } else if (line[0].contains("UBB")) {

                    SimpleEffect simpleEffect = new SimpleEffect();
                    simpleEffect.setName(line[1]);
                    simpleEffect.setPercentValue(Double.parseDouble(line[2]));
                    simpleEffect.setNumberOfTurns(Integer.parseInt(line[3]));
                    simpleEffect.setTarget(Integer.parseInt(line[4]));
                    simpleEffect.setPercentProbablity(Double.parseDouble(line[5]));
                    currentUnit.addToUBBList(simpleEffect);

                }else if (line[0].contains("BB")) {

                    SimpleEffect simpleEffect = new SimpleEffect();
                    simpleEffect.setName(line[1]);
                    simpleEffect.setPercentValue(Double.parseDouble(line[2]));
                    simpleEffect.setNumberOfTurns(Integer.parseInt(line[3]));
                    simpleEffect.setTarget(Integer.parseInt(line[4]));
                    simpleEffect.setPercentProbablity(Double.parseDouble(line[5]));
                    currentUnit.addToBBList(simpleEffect);

                }
            }

            fileReader.close();

            return currentUnit;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } {

        }
        return null;
    }

    private void initSpheresList() {

    }

    private void initElgifsList() {

    }

    public ArrayList<Personnage> getUnitsList() {
        return unitsList;
    }
}
