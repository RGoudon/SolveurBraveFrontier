baseStat = 12000    #Stat de base avec les imps et les amélioration SP
flatStat = 0    #

leader1 = 200/100
leader2 = 200/100

sphere1 = 75/100
sphere2 = 25/100

elemItem = 0
nonElemItem = 0

elemBB = 225/100 + 225/100
elemUBB = 500/100

nonElemBB = 190/100
nonElemUBB = 0

tauntBuff = 0

extraSkillBB = 120/100
extraSkillElgif = 10/100

statusEffect = 0
statusAlimentEffect = 150/100

BBMod = 1500/100 +350/100
overDrive = 1


TotalStat = (baseStat + flatStat) * (1 + leader1 + leader2 + sphere1 + sphere2 +
             elemItem + elemBB + elemUBB + nonElemItem + nonElemBB + nonElemUBB 
             + tauntBuff + extraSkillBB + extraSkillElgif + statusEffect + statusAlimentEffect + BBMod)


print("Les dégats de Giselle en SBB sur un mob ayant un debuff de statut est de ", TotalStat)

print("70 '%' de 45 000 = ", 70*45000 / 100)