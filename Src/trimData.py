from os import listdir

dataToTrim = "C:\\Users\\gloumii\\Desktop\\BraveFrontier\\Solveur\\DataToTrim"
dataReady = "C:\\Users\\gloumii\\Desktop\\BraveFrontier\\Solveur\\DataReady\\"

def getFilesList(dataToTrim):

    listFiles = listdir(dataToTrim)

    print("on print la liste des fichiers")

    print(listFiles)

    return listFiles


def trimData(currentFile):


    f = open(currentFile, "r")

    content = []

    text = f.readlines()
    
    for line in text:
        if (",0,0,0" not in line):
            if(line != "\n"):
                content.append(line)
    
    return content

def makeTrimedFile(trimedFileName, trimedData):

    print("on crée le nouveau fichier" , trimedFileName)

    f = open(trimedFileName, "w")

    for line in trimedData:
        f.write(line)
    
    f.close()

def main():

    listFiles = getFilesList(dataToTrim)

    for currentFile in listFiles:
        trimedData = trimData(dataToTrim + "\\" + currentFile)
        trimedFileName = dataReady + currentFile
        makeTrimedFile(trimedFileName, trimedData)




main()